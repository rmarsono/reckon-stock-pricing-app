import React from 'react'

import Log from './Log'
import Summary from './Summary'
import StateContext from './StateContext'
import { formatDate } from '../helpers'

export default class App extends React.Component {
  static contextType = StateContext

  intervalId = 0

  componentDidMount() {
    this.intervalId = setInterval(() => {
      this.getStockPrices()
    }, 2000)
  }

  componentWillUnmount() {
    clearInterval(this.intervalId)
  }

  getStockPrices = () => {
    fetch('https://join.reckon.com/stock-pricing')
      .then(response => response.json())
      .then(data => {
        const { state, logStockPrices } = this.context
        const { isPaused } = state
        const stockPrices = {
          isShown: !isPaused,
          data
        }
        logStockPrices(formatDate(new Date()), stockPrices)
      })
      .catch(e => console.log(e))
  }

  render() {
    return (
      <React.Fragment>
        <Log />
        <Summary />
      </React.Fragment>
    )
  }
}
