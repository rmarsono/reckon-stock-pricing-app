import React from 'react'

import StateContext from './StateContext'
import LogItem from './LogItem'

const Log = () => {
  return (
    <StateContext.Consumer>
      {context => {
        const { state, toggleIsPaused } = context
        const { stockPrices } = state
        return (
          <div id='log' className='column-wrap'>
            <div className='column-header'>
              <h2>Log</h2>
              <button className='log-control' onClick={() => toggleIsPaused()}>
                {state.isPaused ? 'Resume' : 'Pause'} log
              </button>
            </div>
            <ul className='log-list'>
              {Object.keys(stockPrices).map(key => {
                return (
                  stockPrices[key].isShown && (
                    <LogItem
                      key={key}
                      date={key}
                      stockPrice={stockPrices[key]}
                    />
                  )
                )
              })}
            </ul>
          </div>
        )
      }}
    </StateContext.Consumer>
  )
}

export default Log
