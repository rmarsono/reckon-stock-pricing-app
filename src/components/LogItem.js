import React from 'react'

const LogItem = props => {
  const { date, stockPrice } = props
  return (
    <li>
      <span>Updates for {date}</span>
      <ul>
        {stockPrice.data.map((stock, index) => {
          return (
            <li key={index}>
              {stock.code}: ${stock.price.toFixed(2)}
            </li>
          )
        })}
      </ul>
    </li>
  )
}

export default LogItem
