import React from 'react'

import StateContext from './StateContext'

export default class StateProvider extends React.Component {
  state = {
    stockPrices: {},
    isPaused: false,
    summary: {}
  }

  toggleIsPaused = () => {
    this.setState({ isPaused: !this.state.isPaused })
  }

  logStockPrices = (date, newStockPrices) => {
    const { summary } = { ...this.state }
    let { stockPrices } = { ...this.state }
    stockPrices = { [date]: newStockPrices, ...stockPrices }
    stockPrices[date] = newStockPrices
    const { data } = newStockPrices
    data.forEach(item => {
      const { code, price } = item
      if (summary.hasOwnProperty(code)) {
        if (summary[code].lowest > price) summary[code].lowest = price
        if (summary[code].highest < price) summary[code].highest = price
      } else {
        summary[code] = {
          starting: price,
          lowest: price,
          highest: price
        }
      }
      summary[code].current = price
    })
    this.setState({ stockPrices, summary })
  }

  render() {
    return (
      <StateContext.Provider
        value={{
          state: this.state,
          logStockPrices: this.logStockPrices,
          toggleIsPaused: this.toggleIsPaused,
          updateSummary: this.updateSummary
        }}
      >
        {this.props.children}
      </StateContext.Provider>
    )
  }
}
