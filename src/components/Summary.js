import React from 'react'

import SummaryItem from './SummaryItem'
import StateContext from './StateContext'

const Summary = () => {
  return (
    <StateContext.Consumer>
      {context => {
        const { summary } = context.state
        return (
          <div id='summary' className='column-wrap'>
            <div className='column-header'>
              <h2>Summary</h2>
            </div>
            <table>
              <thead>
                <tr>
                  <th>Stock</th>
                  <th>Starting</th>
                  <th>Lowest</th>
                  <th>Highest</th>
                  <th>Current</th>
                </tr>
              </thead>

              <tbody>
                {Object.keys(summary).map(key => {
                  return (
                    <SummaryItem key={key} name={key} prices={summary[key]} />
                  )
                })}
              </tbody>
            </table>
          </div>
        )
      }}
    </StateContext.Consumer>
  )
}

export default Summary
