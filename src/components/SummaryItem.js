import React from 'react'

const SummaryItem = props => {
  const { name, prices } = props
  const { starting, lowest, highest, current } = prices
  return (
    <tr>
      <td>{name}</td>
      <td>{starting.toFixed(2)}</td>
      <td>{lowest.toFixed(2)}</td>
      <td>{highest.toFixed(2)}</td>
      <td>{current.toFixed(2)}</td>
    </tr>
  )
}

export default SummaryItem
