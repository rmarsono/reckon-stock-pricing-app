export function formatDate(rawDate) {
  return (
    rawDate.getFullYear() +
    '-' +
    ('0' + rawDate.getMonth()).slice(-2) +
    '-' +
    ('0' + rawDate.getDate()).slice(-2) +
    ' ' +
    ('0' + rawDate.getHours()).slice(-2) +
    ':' +
    ('0' + rawDate.getMinutes()).slice(-2) +
    ':' +
    ('0' + rawDate.getSeconds()).slice(-2)
  )
}
